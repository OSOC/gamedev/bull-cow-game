// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Console/Cartridge.h"
#include "HiddenWordList.h"
#include "BullCowCartridge.generated.h"

struct FBullCowCount
{
	int32 Bulls = 0;
	int32 Cows = 0;
};

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class BULLCOWGAME_API UBullCowCartridge : public UCartridge
{
	GENERATED_BODY()

	public:
	virtual void BeginPlay() override;
	virtual void OnInput(const FString& Input) override;
	void SetUpGame();
	void EndGame(const bool& bWin);
	void ProcessGuess(const FString& Guess);
	static bool IsIsogram(const FString& Word);
	static TArray<FString> GetValidWords(const TArray<FString>& CheckWords);
	FBullCowCount GetBullCows(const FString& Guess) const;

	// Your declarations go below!
	private:
	FString HiddenWord;
	int32 WordLength;
	int32 Lives;
	bool bGameOver;
	TArray<FString> ValidWords;
};
